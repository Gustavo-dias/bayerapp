# Bayer Recomenda
# Feito especialmente para você que deseja cuidar de sua lavoura com mais segurança!

### Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Npm](https://www.npmjs.com/get-npm). 
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/)

### 🎲 Rodando o Aplicativo (Emulador)

```bash
# Clone este repositório
$ git clone

# Acesse a pasta do projeto no terminal/cmd
$ cd bayerapp

# Vá para a pasta RRPlus
$ cd RRPlus

# Instale as dependências
$ npm install

# Execute a aplicação em modo de desenvolvimento
$ ionic cordova run android

# O Aplicativo inciará no emulador aberto
```


### ⚡ Rodando o Aplicativo (Browser - não recomendado)

```bash
# Clone este repositório
$ git clone

# Acesse a pasta do projeto no terminal/cmd
$ cd bayerapp

# Vá para a pasta RRPlus
$ cd RRPlus

# Instale as dependências
$ npm install

# Execute a aplicação em modo de desenvolvimento
$ ng serve

# O Aplicativo poderá ser acessado pelo browser - http://localhost:4200/
```
