import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HerbicidasPage } from './herbicidas.page';

describe('HerbicidasPage', () => {
  let component: HerbicidasPage;
  let fixture: ComponentFixture<HerbicidasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HerbicidasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HerbicidasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
