import { Component, OnInit } from '@angular/core';
import { RecomendacoesHerbicidasService } from 'src/app/core/services/recomendacoes_herbicida_service';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-herbicidas',
  templateUrl: './herbicidas.page.html',
  styleUrls: ['./herbicidas.page.scss'],
})
export class HerbicidasPage implements OnInit {

  public plantas_daninha: any[];
  public planta_daninha: any;
  public culturas: any[];
  public cultura: any;
  public sistemas: any[];
  public sistema: any;
  public regiao: any;
  public regiaoFull: any;
  public regioes: any[] = ["Cerrado", "Sul Alto", "Sul Transição", "Usar Localização"];
  public current: any;

  constructor(public navCtrl: NavController, public herbicidaService: RecomendacoesHerbicidasService) {



  }




  ionViewWillEnter() {

    this.resetPage();
    window.scrollTo(0, 0);

  }

  resetPage() {

    delete this.plantas_daninha;
    delete this.planta_daninha;
    delete this.culturas;
    delete this.cultura;
    delete this.sistemas;
    delete this.sistema;
    delete this.regiao;
    delete this.regiaoFull;
    delete this.current;

    document.querySelector('#daninhas' as any).style.opacity = 0;
    document.querySelector('#cultura' as any).style.opacity = 0;
    document.querySelector('#sistema' as any).style.opacity = 0;

  }

  recomendacaoPage() {

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify([this.current]),
        cultura: this.cultura,
        type: 'Herbicidas',
        newScenario: true,
        informacoes_adicionais: this.current.informacoes_adicionais
      }
    };

    this.navCtrl.navigateForward('recomendacao', navigationExtras);

  }

  ngOnInit() {

    let inputs = document.querySelectorAll('input')
    inputs.forEach(e => {
      e.checked = false
    });

  }
  scrollTo(idItem: any, selected: any) {


    if (idItem == "#daninhas") {

      this.regiaoFull = this.regiao == "Sul Alto" ? "Sul alto (RS/SC/SUL do paraná)" : this.regiao == "Sul Transição" ? "Sul Transição (Ms/Sp/ Oeste do Paraná)" : this.regiao;
      this.plantas_daninha = this.herbicidaService.getPlantaByRegiao(this.regiaoFull);

    }
    else if (idItem == "#cultura") {

      this.culturas = this.herbicidaService.getCulturaByPlanta(this.planta_daninha, this.regiaoFull);

    }
    else if (idItem == "#sistema") {

      console.log("-" + this.cultura + "-" + this.planta_daninha + "-" + this.regiaoFull + "-");

      this.sistemas = this.herbicidaService.getSistemaByCultura(this.cultura, this.planta_daninha, this.regiaoFull);

    }


    console.log('sistemas' + JSON.stringify(this.sistemas));

    this.current = selected;

    document.querySelector(idItem).scrollIntoView({
      behavior: 'smooth',
      offsetTop: -100,
    });

    document.querySelector(idItem).style.opacity = 1

  }
}
