import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  disablePrevBtn = true;
  disableNextBtn = false;

  @ViewChild('mySlider') slides: IonSlides;
  @ViewChild('NextBtn') nextBtn: any;

  getIndex() {
    let prom1 = this.slides.isBeginning();
    let prom2 = this.slides.isEnd();

    Promise.all([prom1, prom2]).then((data) => {
      data[0] ? this.disablePrevBtn = true : this.disablePrevBtn = false;
      data[1] ? this.disableNextBtn = true : this.disableNextBtn = false;
    });
  }
  swipeNext() {

    this.slides.slideNext();
  }
  slideOpts = {
    slidesPerView: 1,
  }
  homePage: HomePage;
  constructor() { }

  ngOnInit() {

  }
}
