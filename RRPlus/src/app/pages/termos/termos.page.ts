import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-termos',
  templateUrl: './termos.page.html',
  styleUrls: ['./termos.page.scss'],
})
export class TermosPage implements OnInit {


  public accept: any;

  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController) { }

  ngOnInit() {
  }

  async prepareAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Confirme os termos de uso',
      message: "Para continuar no aplicativo é necessário aceitar os termos e condições de uso.",
      buttons: ['Ok'],
      cssClass: 'alertStyle'
    });
    return alert;
  }


  openBayer() {

    if (this.accept != true) {

      this.prepareAlert().then(alert => alert.present());
    }
    else {
      this.navCtrl.navigateForward('bayer');
    }

  }

}
