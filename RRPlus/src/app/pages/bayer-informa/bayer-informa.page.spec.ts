import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BayerInformaPage } from './bayer-informa.page';

describe('BayerInformaPage', () => {
  let component: BayerInformaPage;
  let fixture: ComponentFixture<BayerInformaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BayerInformaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BayerInformaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
