import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BayerInformaPage } from './bayer-informa.page';

const routes: Routes = [
  {
    path: '',
    component: BayerInformaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BayerInformaPage]
})
export class BayerInformaPageModule {}
