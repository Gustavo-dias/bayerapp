import { Component, OnInit, Input  } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-modal-guia',
  templateUrl: './modal-guia.page.html',
  styleUrls: ['./modal-guia.page.scss'],
})
export class ModalGuiaPage implements OnInit {
  @Input() titulo: string;
  @Input() subtitulo: string;
  @Input() descricao: string;

  constructor(navParams: NavParams) {
    // componentProps can also be accessed at construction time using NavParams
    console.log(navParams.get('titulo'))
  }

  ngOnInit() {
  }

}
