import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavParams, ModalController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";

@Component({
  selector: 'app-image-modal',
  templateUrl: './image-modal.page.html',
  styleUrls: ['./image-modal.page.scss'],
})
export class ImageModalPage  {

  @ViewChild('slider', { read: ElementRef })slider: ElementRef;
  img: any;

  sliderOpts = {
    zoom: {
      maxRatio: 5
    }
  };

  constructor(private location: Location,	private route: ActivatedRoute) {

    this.route.queryParams.subscribe(params => {
			this.img =  params.img;
    });

   }
 

  zoom(zoomIn: boolean) {
    let zoom = this.slider.nativeElement.swiper.zoom;
    if (zoomIn) {
      zoom.in();
    } else {
      zoom.out();
    }
  }

  close() {
    this.location.back();
  }

}