import { Component, OnInit } from '@angular/core';
import { HistoricService } from 'src/app/core/services/historic_service';

@Component({
  selector: 'app-historico',
  templateUrl: './historico.page.html',
  styleUrls: ['./historico.page.scss'],
})
export class HistoricoPage implements OnInit {

  public historics: any[] = [];

  constructor(private historicService: HistoricService) { }


  groupBy(colecao, propriedade) {
    var agrupado = [];
    colecao.forEach(function (i) {
      var foiAgrupado = false;
      agrupado.forEach(function (j) {
        if (j.Key == i[propriedade]) {
          j.Elements.push(i);
          foiAgrupado = true;
        }
      });
      if (!foiAgrupado) agrupado.push({ Key: i[propriedade], Elements: [i] });
    });
    return agrupado;
  }


  ngOnInit() {

    this.historicService.getHistorics().then(data => {

      var itens: any = data;

      if (itens != undefined) {

        if (itens[0].group == undefined) {

          itens = [{
            date: itens[0],
            day: itens[1],
            group: itens[2],
            monthName: itens[3],
            monthNameShort: itens[4],
            text: itens[5],
            title: itens[6],
            uuid: itens[7],
            weekName: itens[8],
            year: itens[9]
          }];

        }
        
      }

      var grouped = this.groupBy(itens, 'group');
      this.historics = grouped;

    });

  }

}
