import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import Swal from 'sweetalert2';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { ScenarioService } from 'src/app/core/services/scenario_service';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-meus-cenarios',
  templateUrl: './meus-cenarios.page.html',
  styleUrls: ['./meus-cenarios.page.scss'],
})
export class MeusCenariosPage implements OnInit {
  @ViewChild('sliderBoasPraticas') slides: IonSlides;

  public scenarios: any[];

  public scenariosFavoritos: any[];
  public scenariosHistorico: any[];

  constructor(public alertCtrl: AlertController, public alertController: AlertController, public navCtrl: NavController, public toastController: ToastController, public scenarioService: ScenarioService) {


  }


  getData() {

    this.scenarioService.getScenarios().then(data => {

      if (data && data[0]) {

        var itens: any = data;

        this.scenariosHistorico = itens;
        this.scenariosFavoritos = itens.filter(function (a) { return a.favorite === true; });

      }

    });

  }

  ionViewDidEnter() {

    this.getData();

  }


  async editName(item: any) {
    let alert = await this.alertCtrl.create({
      message: 'Nome do cenário',
      subHeader: 'Editar cenário',
      inputs: [
        {
          name: 'title',
          placeholder: 'Nome do cenário',
          value: item.title

        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salvar',
          handler: data => {

            this.scenarioService.updateNameScenario(item.id, data.title).then((value) => {

              this.getData();

            });
          }
        }
      ]
    });

    await alert.present();
  }

  recomendacaoPage(item: any) {


    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(item.data),
        cultura: item.cultura,
        type: item.type,
        newScenario: false,
        text: item.textRecomendacao,
        id: item.id
      }
    };

    this.navCtrl.navigateForward('recomendacao', navigationExtras);
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Item excluído.',
      duration: 2000
    });
    toast.present();
  }
  async confirmDelete(id: any) {
    const alert = await this.alertController.create({
      header: 'Excluir Cenário',
      message: 'Deseja realmente <strong>excluir</strong> este item ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: Item excluído');
          }
        }, {
          text: 'Sim',
          handler: () => {
            this.scenarioService.removeScenario(id).then((excluded) => {

              this.getData();
              this.presentToast();

            });

          }
        }
      ]
    });

    await alert.present();
  }

  alertSwal() {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })
  }
  artigos() {
    this.slides.slideTo(0);
  }

  videos() {
    this.slides.slideTo(1);
  }
  slideOpts = {
    slidesPerView: 1,
    spaceBetween: 0,
    watchSlidesProgress: false,
    speed: 200,
    noSwiping: true
  }

  ngOnInit() {
  }

}
