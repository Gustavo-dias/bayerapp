import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeusCenariosPage } from './meus-cenarios.page';

describe('MeusCenariosPage', () => {
  let component: MeusCenariosPage;
  let fixture: ComponentFixture<MeusCenariosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeusCenariosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeusCenariosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
