import { Component, OnInit, Directive, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { RecomendacoesService } from 'src/app/core/services/recomendacoes_services';
import { NavController, AlertController, IonContent } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-inceticidas',
  templateUrl: './inceticidas.page.html',
  styleUrls: ['./inceticidas.page.scss']
})


export class InceticidasPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;

  cultura: string;
  estadioSelectedArray: string[];
  culturaSelectedArray: string[];

  tratamento_semente: string;
  estadio_cultivo: string;
  culturas: any[] = ['Milho', 'Soja'];
  estadio_cultivos: any[] = [];
  selected: any;
  estadio_cultivosMilho: any[] = ['V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8', 'V9'];
  estadio_cultivosSoja: any[] =
    ['V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'VN',
      'R1', 'R2', 'R3', 'R4', 'R5', 'R6'];

  tratamento_sementes: any[] = [];
  tratamento_sementesMilho: any[] = [
    { name: 'Carbamato', status: false, id: '1' },
    { name: 'Neonicotinoide', status: false, id: '2' },
    { name: 'Diamida', status: false, id: '3' },
    { name: 'Fipronil', status: false, id: '4' },
    { name: 'Nenhum', status: false, id: '5' }];

  tratamento_sementesSoja: any[] = [
    { name: 'Fipronil', status: false, id: '6' },
    { name: 'Carbamato', status: false, id: '7' },
    { name: 'Neonic', status: false, id: '8' },
    { name: 'Diamida', status: false, id: '9' },
    { name: 'CropStar', status: false, id: '10' },
    { name: 'Nenhum', status: false, id: '10' }];


  dessecacao_soja: any[] = [
    { name: 'Carbamato', status: false, id: '11' },
    { name: 'Piretróide', status: false, id: '12' },
    { name: 'Diamida', status: false, id: '13' },
    { name: 'Nenhum', status: false, id: '14' }];


  pragas: any[] = [];
  praga: any;
  davi: any;
  escalaLagarto: any[] = [];
  escalaPercevejo: any[] = [];
  injuria: any;
  cigarrinhaId: any;

  constructor(
    public recomendacoesService: RecomendacoesService,
    public navCtrl: NavController,
    private alertCtrl: AlertController
  ) { }


  ionViewWillEnter() {

    this.resetPage(true);

  }

  ngOnInit() {

    let estadio = document.querySelector('#estadio')
    let inputs = estadio.querySelectorAll('input')
    let pragas = document.querySelector('#pragas')

    this.estadioSelectedArray = [];
    this.culturaSelectedArray = [];

    inputs.forEach(e => {
      e.addEventListener('click', function () {
        document.querySelector('#pragas').scrollIntoView({
          behavior: 'smooth',
        });
        // document.querySelector('#pragas').style.opacity = 1;
      })
    })
  }

  async prepareAlert(text: any) {
    const alert = await this.alertCtrl.create({
      header: 'Refúgio',
      message: text,
      buttons: ['Ok'],
      cssClass: 'alertStyle'
    });
    return alert;
  }

  resetPage(hiddeCultura: boolean) {

    delete this.estadio_cultivo;
    delete this.tratamento_semente;
    delete this.estadio_cultivo;
    delete this.estadio_cultivos;
    delete this.selected;
    delete this.tratamento_sementes;
    delete this.pragas;
    delete this.praga;
    delete this.davi;
    delete this.escalaLagarto;
    delete this.escalaPercevejo;
    delete this.injuria;
    delete this.cigarrinhaId;

    document.querySelector('#estadio' as any).style.display = 'none';
    document.querySelector('#escala_injuria' as any).style.display = 'none';
    document.querySelector('#escala' as any).style.display = 'none';
    document.querySelector('#perguntaDessecacao' as any).style.display = 'none';


    if (hiddeCultura) {
      var pragasElement = document.querySelector('#pragas' as any);
      if (pragasElement) {
        pragasElement.style.display = 'none';
      }
      delete this.cultura;
    }

    document.querySelector('#pergunta' as any).style.display = 'none';
    document.querySelector('#submit-button' as any).style.display = 'none';



  }


  pragasMilho() {
    return this.recomendacoesService.pragasMilho(this.tratamento_semente, this.estadio_cultivo);
  }


  pragasSoja() {
    return this.recomendacoesService.pragasSoja(this.tratamento_semente, this.estadio_cultivo);
  }

  getEscalaInjuria(praga) {
    return this.recomendacoesService.escalaMilho(this.tratamento_semente, this.estadio_cultivo, praga, 'escala_injuria');
  }

  getEscalaDavis(praga) {
    return this.recomendacoesService.escalaMilho(this.tratamento_semente, this.estadio_cultivo, praga, 'escala_davis');
  }




  recomendacaoPage() {

    console.log(JSON.stringify(this.recomendacoesService.getMilhoById([this.injuria, this.cigarrinhaId], this.davi)));
    
    console.log(this.injuria + '-' + this.cigarrinhaId + '-' + this.davi);

    if (this.cultura == 'Milho') {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          data: JSON.stringify(this.recomendacoesService.getMilhoById([this.injuria, this.cigarrinhaId], this.davi)),
          cultura: this.cultura,
          tratamento_semente: this.tratamento_semente,
          estadio_cultivo: this.estadio_cultivo,
          type: 'Inceticidas',
          newScenario: true
        }
      };

      this.navCtrl.navigateForward('recomendacao', navigationExtras);

    }
    else if (this.cultura == 'Soja') {

      var itens = [];

      for (let i = 0; i < this.pragas.length; i++) {

        if (this.pragas[i].status == true) {

          itens.push(this.pragas[i].id);
        }
      }

      console.log(JSON.stringify(itens));

      let navigationExtras: NavigationExtras = {
        queryParams: {
          data: JSON.stringify(this.recomendacoesService.getSojaById(itens)),
          cultura: this.cultura,
          tratamento_semente: this.tratamento_semente,
          estadio_cultivo: this.estadio_cultivo,
          type: 'Inceticidas',
          newScenario: true
        }
      };

      this.navCtrl.navigateForward('recomendacao', navigationExtras);

    }

  }


  checkFinalButton() {

    if (this.pragas !== undefined) {
      if (this.cultura == 'Milho') {

        if (this.pragas.length >= 3) {

          if (this.pragas[0].status == true && this.pragas[1].status == true) {

            if (this.davi && this.injuria) {

              document.querySelector('#submit-button' as any).style.display = 'block';

            }
            else {

              document.querySelector('#submit-button' as any).style.display = 'none';
            }

          }
          else if (this.pragas[0].status == true) {

            if (this.davi) {
              document.querySelector('#submit-button' as any).style.display = 'block';
            }
            else {
              document.querySelector('#submit-button' as any).style.display = 'none';
            }

          }
          else if (this.pragas[1].status == true) {

            if (this.injuria) {
              document.querySelector('#submit-button' as any).style.display = 'block';
            }
            else {
              document.querySelector('#submit-button' as any).style.display = 'none';
            }
          }
          else if (this.pragas[2].status == true) {

            document.querySelector('#submit-button' as any).style.display = 'block';

          }
          else {

            document.querySelector('#submit-button' as any).style.display = 'none';

          }


        }
        else if (this.pragas.length >= 2) {

          if (this.pragas[0].status == true && this.pragas[1].status == true) {

            if (this.davi && this.injuria) {

              document.querySelector('#submit-button' as any).style.display = 'block';

            }
            else {

              document.querySelector('#submit-button' as any).style.display = 'none';
            }

          }
          else if (this.pragas[0].status == true) {

            if (this.davi) {
              document.querySelector('#submit-button' as any).style.display = 'block';
            }
            else {
              document.querySelector('#submit-button' as any).style.display = 'none';
            }

          }
          else if (this.pragas[1].status == true) {

            if (this.injuria) {
              document.querySelector('#submit-button' as any).style.display = 'block';
            }
            else {
              document.querySelector('#submit-button' as any).style.display = 'none';
            }
          }

          else {

            document.querySelector('#submit-button' as any).style.display = 'none';

          }


        }
        else if (this.pragas.length >= 1) {


          if (this.pragas[0].status == true) {

            if (this.davi) {
              document.querySelector('#submit-button' as any).style.display = 'block';
            }
            else {
              document.querySelector('#submit-button' as any).style.display = 'none';
            }

          }

          else {

            document.querySelector('#submit-button' as any).style.display = 'none';

          }


        }



      }
      else if (this.cultura == 'Soja') {

        var status = false;

        for (let i = 0; i < this.pragas.length; i++) {

          if (this.pragas[i].status == true) {
            status = true;
          }

        }

        if (status) {
          document.querySelector('#submit-button' as any).style.display = 'block';
        }
        else {
          document.querySelector('#submit-button' as any).style.display = 'none';

        }


      }

    }

  }


  multipleSelect(array: any[], position: number, name: any) {

    if (array[position].status && name == 'Nenhum') {

      for (let index = 0; index < array.length - 1; index++) {

        array[index].status = false;
        (document.getElementById(array[index].name + array[index].id) as HTMLInputElement).checked = false
      }
    }
    else {

      (document.getElementById(array[position].name + array[position].id) as HTMLInputElement).checked = false
      array[position].status = false;

    }
  }



  alertRefugio(selected: any) {

    if (selected) {

      if (selected == true) {

        var alertMessage = this.recomendacoesService.alertRefugio(this.cultura);

        if (alertMessage.length > 0) {

          this.prepareAlert(alertMessage[0].refugio_alert).then(alert => alert.present());
        }

      }

    }

  }

  scrollTo(idItem: any, selected: any) {
    if (idItem == "#pergunta") {
      this.resetPage(false);
      this.alertRefugio(selected);
      if (selected.name) {
        if (this.culturaSelectedArray.find(item => item === selected.name)) {
          const index = this.culturaSelectedArray.findIndex(item => item === selected.name)
          this.culturaSelectedArray.splice(index, 1)
        } else {
          this.culturaSelectedArray.push(selected.name)
        }
      }

      document.querySelector('#pergunta' as any).style.display = 'block';

      if (this.cultura == "Milho") {
        this.tratamento_sementes = this.tratamento_sementesMilho;
        this.estadio_cultivos = this.estadio_cultivosMilho;
      }
      else if (this.cultura == "Soja") {

        this.tratamento_sementes = this.tratamento_sementesSoja;
        this.estadio_cultivos = this.estadio_cultivosSoja;
        document.querySelector('#perguntaDessecacao' as any).style.display = 'block';

        this.multipleSelect(this.dessecacao_soja, 3, selected.name);
      }

      if (this.cultura == "Soja" && selected == true) {

        document.querySelector('#perguntaDessecacao' as any).style.display = 'block';
        idItem = '#perguntaDessecacao';

      }

    }

    else if (idItem == '#pragas') {

      console.log('aqui oh');
      if (this.cultura == "Milho") {

        this.pragas = this.pragasMilho();

      }
      else if (this.cultura == "Soja") {

        this.pragas = this.pragasSoja();

        console.log(JSON.stringify(this.pragas));

      }

    }
    else if (idItem == '#escala') {


      if (this.cultura == "Milho") {

        if (selected.praga == 'Lagarta-do-cartucho' && selected.status == true) {


          this.escalaLagarto = this.getEscalaDavis(selected.praga);
          document.querySelector('#escala' as any).style.display = 'block';

        }
        else if (selected.praga == 'Lagarta-do-cartucho' && selected.status == false) {

          document.querySelector('#escala' as any).style.display = 'none';

        }
        else if (selected.praga == 'Percevejo' && selected.status == true) {


          this.escalaPercevejo = this.getEscalaInjuria(selected.praga);

          idItem = "#escala_injuria";
          document.querySelector('#escala_injuria' as any).style.display = 'block';
        }
        else if (selected.praga == 'Percevejo' && selected.status == false) {


          idItem = "#escala_injuria";
          document.querySelector('#escala_injuria' as any).style.display = 'none';
        }
        else if (selected.praga == 'Cigarrinha' && selected.status == true) {

          this.cigarrinhaId = selected.id;

        }
        else if (selected.praga == 'Cigarrinha' && selected.status == false) {

          this.cigarrinhaId = '';

        }

      }
      else if (this.cultura == "Soja") {



      }

    }
    else if (idItem == "#estadio") {
      if (this.estadioSelectedArray.find(item => item === selected.name)) {
        const index = this.estadioSelectedArray.findIndex(item => item === selected.name)
        this.estadioSelectedArray.splice(index, 1)
      } else {
        this.estadioSelectedArray.push(selected.name)
      }
      document.querySelector('#estadio' as any).style.display = 'block';

      if (this.cultura == 'Milho') {

        this.multipleSelect(this.tratamento_sementes, 4, selected.name);

      }
      else if (this.cultura == 'Soja') {

        this.multipleSelect(this.tratamento_sementes, 5, selected.name);

      }

    }


    this.checkFinalButton();

    document.querySelector(idItem).scrollIntoView({
      behavior: 'smooth'
    });

    //document.querySelector(idItem).style.display = 'block';
    document.querySelector(idItem).style.opacity = 1;

  }


}


