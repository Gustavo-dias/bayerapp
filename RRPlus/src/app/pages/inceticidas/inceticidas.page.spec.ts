import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InceticidasPage } from './inceticidas.page';

describe('InceticidasPage', () => {
  let component: InceticidasPage;
  let fixture: ComponentFixture<InceticidasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InceticidasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InceticidasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
