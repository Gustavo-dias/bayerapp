import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { Validators, FormControl, ReactiveFormsModule, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-aceito',
	templateUrl: './aceito.page.html',
	styleUrls: ['./aceito.page.scss'],
})

// this.emailForm = new Form({
	// email: ['', Validators.compose([Validators.maxLength(70), 
	// 	Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), 
	// 	Validators.required])]
// });

export class AceitoPage implements OnInit {

	public email = '';
	public form :any;

constructor(public loadingController: LoadingController, public toastController: ToastController) { 
	this.form = new FormGroup({
		email: new FormControl('', Validators.compose([
			Validators.required,
			Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
		]))
	});
}


ngOnInit() {
}

async presentToast() {
	const toast = await this.toastController.create({
		message: 'E-mail cadastrado com sucesso !',
		duration: 2000,
		color: 'success',
		showCloseButton: true,
	});
	toast.present();
}
async presentLoading() {
	const loading = await this.loadingController.create({
		message: 'Salvando',
		duration: 2000
	});
	await loading.present();
	const { role, data } = await loading.onDidDismiss();
	this.presentToast()

}

async onSubmit() {
	console.log(this)
}

}
