import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceitoPage } from './aceito.page';

describe('AceitoPage', () => {
  let component: AceitoPage;
  let fixture: ComponentFixture<AceitoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceitoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceitoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
