import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-video',
  templateUrl: './video.page.html',
  styleUrls: ['./video.page.scss'],
})
export class VideoPage implements OnInit {



  constructor() {
  }

  ngOnInit() {

  }

  slideOpts = {
    slidesPerView: 1,
    spaceBetween: 0,
    watchSlidesProgress: false,
    speed: 200,
    noSwiping: true
  }
}
