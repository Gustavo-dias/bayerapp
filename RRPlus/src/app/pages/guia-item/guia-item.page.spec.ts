import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuiaItemPage } from './guia-item.page';

describe('GuiaItemPage', () => {
  let component: GuiaItemPage;
  let fixture: ComponentFixture<GuiaItemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuiaItemPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuiaItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
