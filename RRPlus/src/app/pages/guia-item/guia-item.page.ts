import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';


@Component({
	selector: 'app-guia-item',
	templateUrl: './guia-item.page.html',
	styleUrls: ['./guia-item.page.scss'],
})
export class GuiaItemPage implements OnInit {
	@ViewChild('sliderGuia') slides: IonSlides;
	public data: any;
	public type: any;
	public stage: number;

	slideOpts = {
		slidesPerView: 1,
		spaceBetween: 0,
		watchSlidesProgress: true,
		speed: 500,
		slideShadows: true
	}
	constructor(private route: ActivatedRoute) {

		this.route.queryParams.subscribe(params => {

			this.data = JSON.parse(params["data"]);
			this.type = params["type"];

		});
	}

	handleChange() {
		this.slides.getActiveIndex()
			.then(value => {
				this.stage = value+1
			})
	}

	ngOnInit() {
		this.stage = 1;
	}

}
