import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtigoPage } from './artigo.page';

describe('ArtigoPage', () => {
  let component: ArtigoPage;
  let fixture: ComponentFixture<ArtigoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtigoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtigoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
