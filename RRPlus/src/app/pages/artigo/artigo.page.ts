import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-artigo',
  templateUrl: './artigo.page.html',
  styleUrls: ['./artigo.page.scss'],
})
export class ArtigoPage implements OnInit {

  constructor() { }

  slideOpts = {
    slidesPerView: 1,
    spaceBetween: 0,
    watchSlidesProgress: false,
    speed: 200,
    noSwiping: true
  }

  ngOnInit() {
  }

}
