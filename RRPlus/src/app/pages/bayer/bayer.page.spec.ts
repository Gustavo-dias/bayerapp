import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BayerPage } from './bayer.page';

describe('BayerPage', () => {
  let component: BayerPage;
  let fixture: ComponentFixture<BayerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BayerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BayerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
