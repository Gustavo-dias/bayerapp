import { Component, OnInit, ViewChild  } from '@angular/core';
import { ModalController} from '@ionic/angular';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-recomendacoes',
  templateUrl: './recomendacoes.page.html',
  styleUrls: ['./recomendacoes.page.scss'],
})
export class RecomendacoesPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  dummyList:any;

  constructor(
    ){
      this.dummyList = [
        {
          value:"Esteban Gutmann IV",
        },{
          value:"Bernardo Prosacco Jr.",
        },{
          value:"Nicholaus Kulas PhD",
        },{
          value:"Jennie Feeney",
        },{
          value:"Shanon Heaney",
        },{
          value:"Bernardo Prosacco Jr.",
        },{
          value:"Nicholaus Kulas PhD",
        },{
          value:"Jennie Feeney",
        },{
          value:"Shanon Heaney",
        },{
          value:"Bernardo Prosacco Jr.",
        },{
          value:"Nicholaus Kulas PhD",
        },{
          value:"Jennie Feeney",
        },{
          value:"Shanon Heaney",
        },{
          value:"Bernardo Prosacco Jr.",
        },{
          value:"Nicholaus Kulas PhD",
        },{
          value:"Jennie Feeney",
        },{
          value:"Shanon Heaney",
        },{
          value:"Bernardo Prosacco Jr.",
        },{
          value:"Nicholaus Kulas PhD",
        },{
          value:"Jennie Feeney",
        },{
          value:"Shanon Heaney",
        },{
          value:"Bernardo Prosacco Jr.",
        },{
          value:"Nicholaus Kulas PhD",
        },{
          value:"Jennie Feeney",
        },{
          value:"Shanon Heaney",
        }
      ];
    }
  
    logScrollStart(){
      console.log("logScrollStart : When Scroll Starts");
    }
  
    logScrolling(){
      console.log("logScrolling : When Scrolling");
    }
  
    logScrollEnd(){
      console.log("logScrollEnd : When Scroll Ends");
    }
  
    ScrollToBottom(){
      this.content.scrollToBottom(1500);
    }
  
    ScrollToTop(){
      this.content.scrollToTop(1500);
    }
  
    ScrollToPoint(X,Y){
      this.content.scrollToPoint(X,Y,1500);
    }
    scrollTo(elementId: any) {
      
      let y = document.getElementById(elementId).offsetTop;
      this.content.scrollToPoint(0, y, 2000);
  }

  ngOnInit() {
  }


}
