import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecomendacoesPage } from './recomendacoes.page';

describe('RecomendacoesPage', () => {
  let component: RecomendacoesPage;
  let fixture: ComponentFixture<RecomendacoesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecomendacoesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecomendacoesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
