import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ScenarioService } from 'src/app/core/services/scenario_service';
import { NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { RecomendacoesHerbicidasService } from 'src/app/core/services/recomendacoes_herbicida_service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public scenariosHistorico: any[];
  public scenariosHistoricoFilter: any[];
  public currentValue: any;

  public title = "Bayer Recomendações"
  public homeText = "Para alcançar os melhores resultados com a lavoura, é preciso investir em boas sementes e principalmente na realização de um bom manejo. Clique nos ícones abaixo e veja de que forma proteger sua produtividade."
  constructor(private storage: Storage, public navCtrl: NavController, public scenarioService: ScenarioService) {

 

    this.scenarioService.getScenarios().then(data => {

      if (data && data[0]) {

        var itens: any = data;
        this.scenariosHistorico = itens;
        this.scenariosHistoricoFilter = itens;


        console.log(JSON.stringify(this.scenariosHistoricoFilter));
      }

    });

  }


  ionViewWillEnter() {

    this.storage.get('homePage').then((data) => {

      if (!data || data == false) {

        this.navCtrl.navigateRoot('advertencias');

      }

      this.storage.set('homePage', true);

    });

  }


  open(item: any) {


    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(item.data),
        cultura: item.cultura,
        type: item.type,
        newScenario: false,
        text: item.textRecomendacao,
        id: item.id
      }
    };


    this.navCtrl.navigateForward('recomendacao', navigationExtras);
  }

  getItems(ev: any) {

    var items = this.scenariosHistoricoFilter;
    const val = ev.target.value;

    this.currentValue = val;



    if (val && val.trim() != '') {
      this.scenariosHistorico = items.filter((item) => {
        return (
          item.type.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.textRecomendacao.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.title.toString().toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }

    else {
      this.scenariosHistorico = this.scenariosHistoricoFilter;
    }

  }




  toogleSearchInput() {
    let input = document.getElementById("search-main")
    let searchLogo = document.getElementById("logo-main")
    if (input.style.display == 'block') {
      input.style.display = 'none'
      // searchLogo.style.display = 'block'
    } else {
      input.style.display = 'block'
      // searchLogo.style.display = 'none'
    }
  }

  ngOnInit() {

  }


}
