import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Platform } from '@ionic/angular';


@Component({
	selector: 'app-praticas',
	templateUrl: './praticas.page.html',
	styleUrls: ['./praticas.page.scss'],
})
export class PraticasPage implements OnInit {
	public selectedArc: string;
	public devWidth = this.platform.width();
	public showMore = false;

	@ViewChild('sliderBoasPraticas') slides: IonSlides;
	slideOpts = {
		slidesPerView: 1,
		spaceBetween: 0,
		watchSlidesProgress: false,
		speed: 200,
		noSwiping: true
	};

	ngOnInit(): void {
		this.selectedArc = 'Caminho_3956';
	}

	constructor(public platform: Platform) { }
	artigos() {
		this.slides.slideTo(0);
	}

	handleClick(target) {
		this.showMore = false
		this.selectedArc = target;
	}

	handleShowMore(){
		this.showMore = true;
	}

	videos() {
		this.slides.slideTo(1);
	}

}
