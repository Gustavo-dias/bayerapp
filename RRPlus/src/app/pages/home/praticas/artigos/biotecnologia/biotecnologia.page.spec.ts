import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiotecnologiaPage } from './biotecnologia.page';

describe('BiotecnologiaPage', () => {
	let component: BiotecnologiaPage;
	let fixture: ComponentFixture<BiotecnologiaPage>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [BiotecnologiaPage],
			schemas: [CUSTOM_ELEMENTS_SCHEMA],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(BiotecnologiaPage);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
