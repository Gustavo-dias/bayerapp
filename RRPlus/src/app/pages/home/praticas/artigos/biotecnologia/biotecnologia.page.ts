import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-biotecnologia',
	templateUrl: './biotecnologia.page.html',
	styleUrls: ['./biotecnologia.page.scss'],
})
export class BiotecnologiaPage implements OnInit {
	constructor() {}

	slideOpts = {
		slidesPerView: 1,
		spaceBetween: 0,
		watchSlidesProgress: false,
		speed: 200,
		noSwiping: true,
	};

	ngOnInit() {}
}
