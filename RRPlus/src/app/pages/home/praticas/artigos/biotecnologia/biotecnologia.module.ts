import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BiotecnologiaPage } from './biotecnologia.page';

const routes: Routes = [
	{
		path: '',
		component: BiotecnologiaPage,
	},
];

@NgModule({
	imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes)],
	declarations: [BiotecnologiaPage],
})
export class BiotecnologiaPageModule {}
