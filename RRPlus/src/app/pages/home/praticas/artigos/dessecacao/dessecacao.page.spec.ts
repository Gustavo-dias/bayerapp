import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DessecacaoPage } from './dessecacao.page';

describe('DessecacaoPage', () => {
  let component: DessecacaoPage;
  let fixture: ComponentFixture<DessecacaoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DessecacaoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DessecacaoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
