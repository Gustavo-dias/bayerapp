import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dessecacao',
  templateUrl: './dessecacao.page.html',
  styleUrls: ['./dessecacao.page.scss'],
})
export class DessecacaoPage implements OnInit {

  constructor() { }

  slideOpts = {
    slidesPerView: 1,
    spaceBetween: 0,
    watchSlidesProgress: false,
    speed: 200,
    noSwiping: true
  }

  ngOnInit() {
  }

}
