import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-controle',
	templateUrl: './controle.page.html',
	styleUrls: ['./controle.page.scss'],
})
export class ControlePage implements OnInit {
	constructor() {}

	slideOpts = {
		slidesPerView: 1,
		spaceBetween: 0,
		watchSlidesProgress: false,
		speed: 200,
		noSwiping: true,
	};

	ngOnInit() {}
}
