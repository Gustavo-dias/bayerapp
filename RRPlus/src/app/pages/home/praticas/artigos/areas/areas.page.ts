import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-areas',
	templateUrl: './areas.page.html',
	styleUrls: ['./areas.page.scss'],
})
export class AreasPage implements OnInit {
	constructor() {}

	slideOpts = {
		slidesPerView: 1,
		spaceBetween: 0,
		watchSlidesProgress: false,
		speed: 200,
		noSwiping: true,
	};

	ngOnInit() {}
}
