import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-monitoramento',
  templateUrl: './monitoramento.page.html',
  styleUrls: ['./monitoramento.page.scss'],
})
export class MonitoramentoPage implements OnInit {

  constructor() { }

  slideOpts = {
    slidesPerView: 1,
    spaceBetween: 0,
    watchSlidesProgress: false,
    speed: 200,
    noSwiping: true
  }

  ngOnInit() {
  }

}
