import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PraticasPage } from './praticas.page';

describe('PraticasPage', () => {
  let component: PraticasPage;
  let fixture: ComponentFixture<PraticasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PraticasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PraticasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
