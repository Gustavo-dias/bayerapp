import { Component, OnInit, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { ModalGuiaPage } from '../../modal-guia/modal-guia.page';
import { PlantsService } from 'src/app/core/services/plants_service';
import { NavigationExtras } from '@angular/router';

@Component({
	selector: 'app-guia',
	templateUrl: './guia.page.html',
	styleUrls: ['./guia.page.scss'],
})
export class GuiaPage implements OnInit {
	@ViewChildren('daninhas') daninhasArray: QueryList<any>;
	@ViewChildren('pragas') pragasArray: QueryList<any>;
	public daninhasItens: any[];
	public daninhasItensFilter: any[];
	public daninhasTotalItens: any[];
	public pragasTotalItens: any[];
	public pragasItens: any[];
	public pragasItensFilter: any[];
	public current: any = 'Daninhas';
	public daninhasLoading = true;
	public pragasLoading = true;
	public inputSearch = '';

	slideOpts = {
		slidesPerView: 1,
		spaceBetween: 0,
		watchSlidesProgress: false,
		speed: 200,
		autoHeight: true,
		noSwiping: true
	};

	constructor(public navCtrl: NavController, public modalController: ModalController, public plantsService: PlantsService) {

		this.plantsService.getAll().subscribe(data => {
			if (data && data[0]) {

				this.daninhasTotalItens = data.filter((a) => a.type === 'Daninhas');
				this.daninhasItensFilter = this.daninhasTotalItens;
				this.daninhasItens = this.daninhasTotalItens.slice(0, 8);

				this.pragasTotalItens = data.filter((a) => a.type === 'Pragas');
				this.pragasItensFilter = this.pragasTotalItens;
				this.pragasItens = this.pragasTotalItens.slice(0, 8);
			}

		});
	}

	async presentModal() {
		const modal = await this.modalController.create({
			component: ModalGuiaPage,
			componentProps: {
				'titulo': 'Beldroega',
				'subtitulo': 'Portulaca oleracea (POROL))',
				'descricao': 'epturi sint occaecati cupiditate non provident, similique sunt in culpaqui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.',
				'sliderimgs': ['../assets/img/teste.png', '../assets/img/teste.png', '../assets/img/teste.png'],
			}
		});
		return await modal.present();
	}
	
	daninhas() {
		this.inputSearch = '';
		this.current = 'Daninhas';
	}

	pragas() {
		this.inputSearch = '';
		this.current = 'Pragas';
	}



	details(item: any) {

		let navigationExtras: NavigationExtras = {
			queryParams: {
				data: JSON.stringify(item),
				type: this.current
			}
		};

		this.navCtrl.navigateForward('guia-item', navigationExtras);

	}


	getItems(ev: any) {


		if (this.current == 'Daninhas') {

			var items = this.daninhasItensFilter;
			const val = ev.target.value;

			if (val && val.trim() != '') {
				this.daninhasItens = items.filter((item) => {
					return (
						item.name.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
						item.details.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
						item.description.toString().toLowerCase().indexOf(val.toLowerCase()) > -1);
				})
			}

			else {
				this.daninhasItens = this.daninhasItensFilter.slice(0, 10);
			}

		}
		else {

			var items = this.pragasItensFilter;
			const val = ev.target.value;

			if (val && val.trim() != '') {
				this.pragasItens = items.filter((item) => {
					return (
						item.name.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
						item.details.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
						item.description.toString().toLowerCase().indexOf(val.toLowerCase()) > -1);
				})
			}

			else {
				this.pragasItens = this.pragasItensFilter;
			}

		}

	}



	daninhasRendered(){
		this.daninhasLoading = false;
	}

	// pragasRendered(){
	// 	console.log('entrou aqui');
	// 	this.pragasLoading = false;
	// 	this.slides.updateAutoHeight();
	// }

	ngOnInit() {
		
	}

	renderMorePragas() {
		switch (this.pragasItens.length) {
			case 8:
				for (let i = 8; i < 16; i++) {
					this.pragasItens.push(this.pragasTotalItens[i]);
				}
				break;
			case 16:
				for (let i = 16; i < 24; i++) {
					this.pragasItens.push(this.pragasTotalItens[i]);
				}
				break;
			case 24:
				for (let i = 24; i < 32; i++) {
					this.pragasItens.push(this.pragasTotalItens[i]);
				}
				break;
			case 32:
				for (let i = 32; i < this.pragasTotalItens.length; i++) {
					this.pragasItens.push(this.pragasTotalItens[i]);
				}
				break;
			default:
				break;
		}
	}
	
	renderMoreDaninhas() {
		switch (this.daninhasItens.length) {
			case 8:
				for (let i = 8; i < 16; i++) {
					this.daninhasItens.push(this.daninhasTotalItens[i]);
				}
				break;
			case 16:
				for (let i = 16; i < 24; i++) {
					this.daninhasItens.push(this.daninhasTotalItens[i]);
				}
				break;
			case 24:
				for (let i = 24; i < this.daninhasTotalItens.length; i++) {
					this.daninhasItens.push(this.daninhasTotalItens[i]);
				}
				break;
			default:
				break;
		}
	}

	loadData(event) {
		setTimeout(() => {
			if (this.current === 'Daninhas') {
				this.renderMoreDaninhas();
			} else {
				this.renderMorePragas();
			}
			event.target.complete();
		}, 500);
	}
	
  
  ngAfterViewInit() {
		// this.pragasArray.changes.subscribe(t => {
		// 	this.pragasRendered();
		// })
		this.daninhasArray.changes.subscribe(t => {
			setTimeout(() => {
				this.daninhasRendered();
			}, 3000)
		})
	}	
}