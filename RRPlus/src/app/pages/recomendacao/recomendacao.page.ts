import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, AlertController, ModalController, NavController } from '@ionic/angular';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import { HistoricService } from 'src/app/core/services/historic_service';
import { ScenarioService } from 'src/app/core/services/scenario_service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { RecomendacoesService } from 'src/app/core/services/recomendacoes_services';
import { ImageModalPage } from '../image-modal/image-modal.page';

@Component({
	selector: 'app-recomendacao',
	templateUrl: './recomendacao.page.html',
	styleUrls: ['./recomendacao.page.scss'],
})
export class RecomendacaoPage {
	constructor(
		private route: ActivatedRoute,
		private historicService: HistoricService,
		private scenarioService: ScenarioService,
		private photoViewer: PhotoViewer,
		private screenOrientation: ScreenOrientation,
		private alertCtrl: AlertController,
		private recomendacoesService: RecomendacoesService,
		private modalController: ModalController,
		public navCtrl: NavController,
	) {
		this.route.queryParams.subscribe(params => {
			this.data = JSON.parse(params.data);
			this.cultura = params.cultura;
			this.tratamentoSemente = params.tratamento_semente;
			this.estagioCultivo = params.estadio_cultivo;
			this.type = params.type;
			this.informacoes_adicionais = params.informacoes_adicionais;
			this.newScenario = params.newScenario;
			this.text = params.text;
			this.id = params.id;
			// console.log('Data Incoming:', this.data);

			// Selects the first data description
			this.stepSelected = 0;
			this.step = 1;

			console.log('data: ' + JSON.stringify(this.data));

			if (this.type === 'Herbicidas') {
				this.itensDescription = [{ name: this.data[0].total[0].name }];
				this.days = this.data[0].total[0].moment;
			} else {
				this.itensDescription = this.data[0].items;
			}

			if (this.type === 'Herbicidas') {
				const noImage = false;
				let { regiao, cultura, sistema, planta_daninha } = this.data[0] as { [key: string]: string };
				console.log({ regiao }, { planta_daninha }, { cultura }, { sistema });

				const normalize = (str: string) => {
					return str
						.trim()
						.toLowerCase()
						.split(' ')
						.join('_')
						.split('/')
						.join('__')
						.split('+')
						.join('__')
						.split('.')
						.join('')
						.split('____')
						.join('__')
						.split('___')
						.join('__')
						.replace(new RegExp('[()]', 'gi'), '')
						.replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'a')
						.replace(new RegExp('[ÉÈÊ]', 'gi'), 'e')
						.replace(new RegExp('[ÍÌÎ]', 'gi'), 'i')
						.replace(new RegExp('[ÓÒÔÕ]', 'gi'), 'o')
						.replace(new RegExp('[ÚÙÛ]', 'gi'), 'u')
						.replace(new RegExp('[Ç]', 'gi'), 'c')
						.split('safra_de_')
						.join('')
						.split('_ms__sp__oeste_do_parana')
						.join('')
						.split('_rs__sc__sul_do_parana')
						.join('');
				};

				regiao = normalize(regiao);
				planta_daninha = normalize(planta_daninha);
				cultura = normalize(cultura);
				sistema = normalize(sistema);

				console.log({ regiao }, { planta_daninha }, { cultura }, { sistema });
				this.imagePrefix = `../../../assets/img/recomendacoes/herbicida/${regiao}/${planta_daninha}/${cultura}/${sistema}`;
				console.log(this.imagePrefix);
			} else {
				const fullIncludes = (array: string[], ...values: string[]): boolean => {
					return values.reduce((has, value) => {
						if (!has) {
							return false;
						}
						return array.includes(value);
					}, true);
				};
				const normalize = (str: string) => {
					return str
						.trim()
						.toLowerCase()
						.replace(new RegExp('[()]', 'gi'), '')
						.replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'a')
						.replace(new RegExp('[ÉÈÊ]', 'gi'), 'e')
						.replace(new RegExp('[ÍÌÎ]', 'gi'), 'i')
						.replace(new RegExp('[ÓÒÔÕ]', 'gi'), 'o')
						.replace(new RegExp('[ÚÙÛ]', 'gi'), 'u')
						.replace(new RegExp('[Ç]', 'gi'), 'c');
				};

				// ainda não tem imagens
				const data = this.data[0];
				console.log({ data });

				const { estadio_cultivo, praga } = data;

				let folder1: string;
				if (fullIncludes(estadio_cultivo, 'V1', 'V2', 'V3')) {
					folder1 = 'v1_v3';
				} else if (fullIncludes(estadio_cultivo, 'V4', 'V5', 'V6')) {
					folder1 = 'v4_v6';
				} else if (fullIncludes(estadio_cultivo, 'V1', 'VN')) {
					folder1 = 'v1_vn';
				} else if (fullIncludes(estadio_cultivo, 'V6', 'VN')) {
					folder1 = 'v6_vn';
				} else if (fullIncludes(estadio_cultivo, 'R1', 'R2', 'R3', 'R4', 'R5', 'R6')) {
					folder1 = 'r1_r6';
				} else if (fullIncludes(estadio_cultivo, 'R1', 'R2', 'R3')) {
					folder1 = 'r1_r3';
				} else if (fullIncludes(estadio_cultivo, 'R1', 'R2', 'R3')) {
					folder1 = 'r3_r4';
				} else {
					folder1 = 'r5';
				}

				let folder2: string;
				switch (normalize(praga)) {
					case 'mosca-branca':
						folder2 = 'mosca-branca';
						break;
					case 'acaros':
						folder2 = 'acaros';
						break;
					case 'percevejos':
						folder2 = 'percevejos';
						break;
					default:
						folder2 = 'outros';
						break;
				}

				this.imagePrefix = `../../../assets/img/recomendacoes/inseticida/${folder1}/${folder2}`;
			}

			this.steps = [];

			if (this.data != undefined) {
				if (this.data.length == 1) {
					this.praga = this.data;
					this.sistema = this.data;
					// this.setHerbicidasSlide();

					// this.changeStep();
				}

				if (!this.text) {
					if (this.type === 'Inceticidas') {
						this.text =
							'Recomendação - ' +
							this.type +
							'/ Cultura - ' +
							this.cultura +
							'/ Pergunta - ' +
							this.tratamentoSemente +
							'/ Estádio cultura - ' +
							this.estagioCultivo;
					} else if (this.type === 'Herbicidas') {
						this.text = 'Recomendação - ' + this.type + '/ Cultura - ' + this.cultura;
					}
				}

				if (this.newScenario === true) {
					this.scenarioService.newScenario(this.data, this.type, this.text, this.cultura).then((data: any) => {
						this.id = data.id;
					});

					this.historicService.newHistoric('Recomendaçōes', this.text);
				}
			}
		});
		this.culturaSelected = true;
	}

	public estacao: any = 'verao';
	private slideCount: any;
	public shouldDisableSelect: number[] = [];
	public data: any;
	public cultura: any;
	public steps: any[] = [];
	public step: any;
	public description: any;
	public name: any;
	public praga: any;
	public pragaAlert: any;
	public tratamentoSemente: any;
	public estagioCultivo: any;
	public type: any;
	public sistema: any;
	public newScenario: any;
	public text: any;
	public id: any;
	public remedios: any;
	stepSelected: any;
	public itensDescription: any;
	public days: any;
	informacoes_adicionais: any;
	culturaSelected: boolean;
	public slidesPerView: any;
	backgroundPositionY: any;
	@ViewChild('mySlider') slides: IonSlides;

	public imagePrefix: string;

	slideOpts = {
		slidesPerView: this.checkScreen(),
		spaceBetween: 10,
		watchSlidesProgress: false,
		speed: 800,
	};

	favoriteScenario() {
		this.scenarioService.favoriteScenario(this.id).then(data => {
			alert('Recomendação favoritada com sucesso!');
		});
	}

	checkScreen() {
		if (window.innerWidth >= 600) {
			this.backgroundPositionY = '220px';
			return 2.2;
		} else {
			this.backgroundPositionY = '190px';
			return 1.0;
		}
	}

	ionViewDidEnter() {
		const element = document.getElementsByClassName('radio')[0] as HTMLElement;
		const slide = document.getElementsByTagName('ion-slide')[0];

		if (element != undefined) {
			element.click();
		}
		if (slide != undefined) {
			slide.click();
		}
	}

	swipeNext() {
		this.slides.slideNext();

		const element = document.getElementsByTagName('ion-slide')[Number(this.stepSelected) + 1];

		if (element !== undefined) {
			const current = Number(this.stepSelected);
			this.stepSelected = current + 1;
			element.click();
		}
	}

	swipePrevious() {
		this.slides.slidePrev();

		const element = document.getElementsByTagName('ion-slide')[Number(this.stepSelected) - 1];

		if (element !== undefined) {
			const current = Number(this.stepSelected);
			this.stepSelected = current - 1;
			element.click();
		}
	}

	swipeTo(number: number) {
		if (number === 6 && this.type === 'Herbicidas') {
			const index = this.data[0].total
				.map(function(e) {
					return e.type;
				})
				.indexOf('inverno');

			this.slides.slideTo(index);
			const slide = document.getElementsByTagName('ion-slide')[index];
			if (slide != undefined) {
				slide.click();
			}
		} else {
			this.slides.slideTo(number);
			const slide = document.getElementsByTagName('ion-slide')[0];
			if (slide != undefined) {
				slide.click();
			}
		}
	}

	showItem(item: any, i: any) {
		if (this.type === 'Inceticidas') {
			this.description = item.desc;
			this.name = item.name;
			this.step = item.janela;
			this.stepSelected = i;
			this.remedios = item.items;
			this.itensDescription = item.items;
			this.days = item.days;
		} else if (this.type === 'Herbicidas') {
			this.description = item.moment;
			this.name = item.name;
			this.step = item.i;
			this.stepSelected = i;
			this.itensDescription = item.items;
			this.days = item.moment;
			this.estacao = item.type;
		}

		this.step = parseInt(i, 10) + 1;
	}

	async prepareAlert(text: any) {
		const alert = await this.alertCtrl.create({
			header: 'Informações adicionais',
			message: text,
			buttons: ['Ok'],
			cssClass: 'alertStyle',
		});
		return alert;
	}

	alertDetails() {
		if (this.type == 'Inceticidas') {
			let praga = '';

			if (this.pragaAlert.length !== undefined) {
				praga = this.pragaAlert[0].praga;
			} else {
				praga = this.pragaAlert.praga;
			}

			if (praga !== '') {
				const alertMessage = this.recomendacoesService.alertRefugioPraga(this.cultura, praga, this.remedios);

				if (alertMessage) {
					this.prepareAlert(alertMessage).then(alert => alert.present());
				}
			}
		} else if (this.type == 'Herbicidas') {
			if (this.informacoes_adicionais) {
				this.prepareAlert(this.informacoes_adicionais).then(alert => alert.present());
			}
		}
	}

	changeStep(item: any) {
		//  var item: any = this.praga;
		this.step = '';
		this.name = '';
		this.description = '';

		this.stepSelected = 0;
		this.pragaAlert = item;

		const recomendationsArray = [];

		if (item.length === 1) {
			item = item[0];
		}

		if (item) {
			if (this.type === 'Inceticidas') {
				this.pragaAlert = item;

				if (this.cultura === 'Milho') {
					this.steps = [];

					this.steps.push(
						{
							janela: 1,
							days: '(0 a 30 dias)',
							items: [
								{
									name: item.janela_1_aplicacao_1,
									desc: item.janela_1_aplicacao_1_desc,
								},
								{
									name: item.janela_1_aplicacao_2,
									desc: '',
								},
								{
									name: item.janela_1_aplicacao_3,
									desc: item.janela_1_aplicacao_3_desc,
								},
							],
						},
						{
							janela: 2,
							days: '(30 a 60 dias)',
							items: [
								{
									name: item.janela_2_aplicacao_1,
									desc: item.janela_2_aplicacao_1_desc,
								},
								{
									name: item.janela_2_aplicacao_2,
									desc: item.janela_2_aplicacao_1_desc,
								},
								{
									name: item.janela_2_aplicacao_3,
									desc: item.janela_1_aplicacao_1_desc,
								},
							],
						},
					);
				} else if (this.cultura === 'Soja') {
					this.steps = [];

					this.steps.push(
						{
							janela: 1,
							days: '(0 a 30 dias)',
							items: [
								{
									name: item.janela_1_aplicacao_1,
									desc: '',
								},
							],
						},
						{
							janela: 2,
							days: '(30 a 60 dias)',
							items: [
								{
									name: item.janela_2_aplicação_1,
									desc: '',
								},
								{
									name: item.janela_2_aplicacao_2,
									desc: '',
								},
							],
						},
						{
							janela: 3,
							days: '(60 a 90 dias)',
							items: [
								{
									name: item.janela_3_aplicacao_1,
									desc: '',
								},
								{
									name: item.janela_3_aplicacao_2,
									desc: '',
								},
							],
						},
						{
							janela: 4,
							days: '(90 a 120 dias)',
							items: [
								{
									name: item.janela_4_aplicacao_1,
									desc: '',
								},
								{
									name: item.janela_4_aplicacao_2,
									desc: '',
								},
							],
						},
					);
				}

				this.steps = this.steps.map((step, index) => {
					const items = step.items.filter(item => item.name.length);
					if (items.length === 0) {
						this.shouldDisableSelect.push(index);
					}
					return {
						...step,
						items,
					};
				});
			} else if (this.type == 'Herbicidas') {
				this.steps = this.data[0];
				console.log('steps' + JSON.stringify(this.steps));
			}

			if (this.cultura == 'Soja' && this.type === 'Inceticidas') {
				if (!this.steps[0].items[0].name) {
					this.stepSelected = 1;
					this.showItem(this.steps[1], this.stepSelected);
				} else {
					this.showItem(this.steps[0], this.stepSelected);
				}
			} else {
				this.showItem(this.steps[0], this.stepSelected);
			}
		}
	}

	verifyIfSholdBeDisabled(index) {
		const result = this.shouldDisableSelect.findIndex(item => item === index);
		return result;
	}

	async testeClick(event) {
		await this.slides.length().then(index => (this.slideCount = index));

		const target = event.path[2];
		this.slides.length();
		this.slides.getActiveIndex().then(index => {
			console.log(this.slides.length());
		});
	}

	showReport() {
		if (this.data[0].report) {
			if (this.data[0].report !== '') {
				const navigationExtras: NavigationExtras = {
					queryParams: {
						img: this.data[0].report,
					},
				};

				this.navCtrl.navigateForward('image-modal', navigationExtras);
			} else {
				this.presentAlert('Essa imagem está indisponível no momento.');
			}
		}
	}

	async presentAlert(msg: string) {
		const alert = await this.alertCtrl.create({
			header: 'Atenção',
			message: msg,
			buttons: ['OK'],
		});

		await alert.present();
	}
}
