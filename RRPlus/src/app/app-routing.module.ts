import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'home',
		pathMatch: 'full',
	},
	{
		path: 'home',
		loadChildren: './pages/home/home.module#HomePageModule',
	},
	{
		path: 'list',
		loadChildren: './pages/list/list.module#ListPageModule',
	},
	{ path: 'notificacoes', loadChildren: './pages/notificacoes/notificacoes.module#NotificacoesPageModule' },
	{ path: 'meus-cenarios', loadChildren: './pages/meus-cenarios/meus-cenarios.module#MeusCenariosPageModule' },
	{ path: 'historico', loadChildren: './pages/historico/historico.module#HistoricoPageModule' },
	{ path: 'bayer-informa', loadChildren: './pages/bayer-informa/bayer-informa.module#BayerInformaPageModule' },
	{ path: 'como-funciona', loadChildren: './pages/como-funciona/como-funciona.module#ComoFuncionaPageModule' },
	{ path: 'ajuda', loadChildren: './pages/ajuda/ajuda.module#AjudaPageModule' },
	{ path: 'home/guia', loadChildren: './pages/home/guia/guia.module#GuiaPageModule' },
	{ path: 'home/recomendacoes', loadChildren: './pages/home/recomendacoes/recomendacoes.module#RecomendacoesPageModule' },
	{ path: 'home/praticas', loadChildren: './pages/home/praticas/praticas.module#PraticasPageModule' },
	{
		path: 'home/praticas/enfezamento-palido-e-vermelho-do-milho-e-a-cigarrinha',
		loadChildren: './pages/home/praticas/artigos/monitoramento/monitoramento.module#MonitoramentoPageModule',
	},
	{
		path: 'home/praticas/comece-bem-com-o-manejo-pre-plantio-e-pos-emergente',
		loadChildren: './pages/home/praticas/artigos/dessecacao/dessecacao.module#DessecacaoPageModule',
	},
	{
		path: 'home/praticas/o-manejo-integrado-de-pragas-em-tres-passos',
		loadChildren: './pages/home/praticas/artigos/biotecnologia/biotecnologia.module#BiotecnologiaPageModule',
	},
	{
		path: 'home/praticas/refugio-principal-ferramenta-para-manejo-de-resistencia-de-insetos-mri',
		loadChildren: './pages/home/praticas/artigos/areas/areas.module#AreasPageModule',
	},
	{
		path: 'home/praticas/como-evitar-e-manejar-tiguera-ou-milho-rr-voluntario',
		loadChildren: './pages/home/praticas/artigos/controle/controle.module#ControlePageModule',
	},
	{ path: 'intro', loadChildren: './pages/intro/intro.module#IntroPageModule' },
	{ path: 'bayer', loadChildren: './pages/bayer/bayer.module#BayerPageModule' },
	{ path: 'advertencias', loadChildren: './pages/advertencias/advertencias.module#AdvertenciasPageModule' },
	{ path: 'termos', loadChildren: './pages/termos/termos.module#TermosPageModule' },
	{ path: 'herbicidas', loadChildren: './pages/herbicidas/herbicidas.module#HerbicidasPageModule' },
	{ path: 'inceticidas', loadChildren: './pages/inceticidas/inceticidas.module#InceticidasPageModule' },
	{ path: 'recomendacao', loadChildren: './pages/recomendacao/recomendacao.module#RecomendacaoPageModule' },
	{ path: 'diagnostico', loadChildren: './pages/diagnostico/diagnostico.module#DiagnosticoPageModule' },
	{ path: 'aceito', loadChildren: './pages/aceito/aceito.module#AceitoPageModule' },
	{ path: 'modal-guia', loadChildren: './pages/modal-guia/modal-guia.module#ModalGuiaPageModule' },
	{ path: 'guia-item', loadChildren: './pages/guia-item/guia-item.module#GuiaItemPageModule' },
	{ path: 'artigo', loadChildren: './pages/artigo/artigo.module#ArtigoPageModule' },
	{ path: 'video', loadChildren: './pages/video/video.module#VideoPageModule' },
  { path: 'image-modal', loadChildren: './pages/image-modal/image-modal.module#ImageModalPageModule' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
	exports: [RouterModule],
})
export class AppRoutingModule {}
