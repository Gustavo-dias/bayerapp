import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HERBICIDAS } from '../data/recomendacoes_herbicidas';


@Injectable({
    providedIn: 'root'
})
export class RecomendacoesHerbicidasService {


    constructor() {


        this.organizeHerbicidas();

    }




    getUnique(arr, comp) {

        const unique = arr
            .map(e => e[comp])
            .map((e, i, final) => final.indexOf(e) === i && i)
            .filter(e => arr[e]).map(e => arr[e]);

        return unique;
    }




    organizeHerbicidas() {

        var groupBy = function (xs, key) {
            return xs.reduce(function (rv, x) {
                (rv[x[key]] = rv[x[key]] || []).push(x);
                return rv;
            }, {});
        };


        for (let i = 0; i < HERBICIDAS.length; i++) {

            HERBICIDAS[i].inverno = [];
            HERBICIDAS[i].verao = [];
            HERBICIDAS[i].total = [];
            var verao = [];
            var inverno = [];


            inverno.push({ name: HERBICIDAS[i].inverno_1, moment: HERBICIDAS[i].momento_inverno_1 })
            inverno.push({ name: HERBICIDAS[i].inverno_2, moment: HERBICIDAS[i].momento_inverno_2 })
            inverno.push({ name: HERBICIDAS[i].inverno_3, moment: HERBICIDAS[i].momento_inverno_3 })
            inverno.push({ name: HERBICIDAS[i].inverno_4, moment: HERBICIDAS[i].momento_inverno_4 })
            inverno.push({ name: HERBICIDAS[i].inverno_5, moment: HERBICIDAS[i].momento_inverno_5 })
            inverno.push({ name: HERBICIDAS[i].inverno_6, moment: HERBICIDAS[i].momento_inverno_6 })
            inverno.push({ name: HERBICIDAS[i].inverno_7, moment: HERBICIDAS[i].momento_inverno_7 })

            verao.push({ name: HERBICIDAS[i].verao_1, moment: HERBICIDAS[i].momento_verao_1 })
            verao.push({ name: HERBICIDAS[i].verao_2, moment: HERBICIDAS[i].momento_verao_2 })
            verao.push({ name: HERBICIDAS[i].verao_3, moment: HERBICIDAS[i].momento_verao_3 })
            verao.push({ name: HERBICIDAS[i].verao_4, moment: HERBICIDAS[i].momento_verao_4 })
            verao.push({ name: HERBICIDAS[i].verao_5, moment: HERBICIDAS[i].momento_verao_5 })
            verao.push({ name: HERBICIDAS[i].verao_6, moment: HERBICIDAS[i].momento_verao_6 })


            var invernoGroup = Object.values(groupBy(inverno, 'moment'));
            var veraoGroup = Object.values(groupBy(verao, 'moment'));


            for (let j = 0; j < invernoGroup.length; j++) {

                var moment = invernoGroup[j][0].moment;

                if (moment != " " && moment != "" && moment != null && moment != undefined) {

                    var items: any = invernoGroup[j];
                    items = items.map(function (item) { return { name: item.name }; });

                    HERBICIDAS[i].inverno.push({ name: name, moment: moment, type: 'inverno', items: items });

                }

            }


            for (let j = 0; j < veraoGroup.length; j++) {

                var moment = veraoGroup[j][0].moment;

                if (moment != " " && moment != "" && moment != null && moment != undefined) {

                    var items: any = veraoGroup[j];
                    items = items.map(function (item) { return { name: item.name }; });

                    HERBICIDAS[i].verao.push({ name: name, moment: moment, type: 'verao', items: items });

                }

            }

            HERBICIDAS[i].total = HERBICIDAS[i].verao.concat(HERBICIDAS[i].inverno);


        }



        return HERBICIDAS;

    }


    getPlantaByRegiao(regiao: string) {

        var itens = HERBICIDAS.filter(function (element) {
            return (element.regiao.indexOf(regiao) > -1);
        });


        for (let index = 0; index < itens.length; index++) {

            var planta_daninha = itens[index].planta_daninha.replace(new RegExp("[èéêë]", 'g'), "e");
            var planta = planta_daninha.replace(/\s/g, "");

            itens[index].plantaUrl = "../../../assets/img/" + planta + '.png';
        }

        return this.getUnique(itens, 'planta_daninha');
    }

    getCulturaByPlanta(planta: string, regiao: string) {

        var itens = HERBICIDAS.filter(function (element) {
            return (element.planta_daninha.indexOf(planta) > -1 && element.regiao.indexOf(regiao) > -1);
        });


        for (let index = 0; index < itens.length; index++) {

            var cultura = itens[index].cultura.replace(/[\u0300-\u036f]/g, "");
            var cult = cultura.replace(/\s/g, "");

            itens[index].imgUrl = "../../../assets/img/" + cult + '.png';
        }

        return this.getUnique(itens, 'cultura');

    }

    getSistemaByCultura(cultura: string, planta: string, regiao: string) {

        var itens = HERBICIDAS.filter(function (element) {
            return (element.cultura.indexOf(cultura) > -1 && element.planta_daninha == planta && element.regiao.indexOf(regiao) > -1);
        });

      
       return this.getUnique(itens, 'sistema');

    }





}
