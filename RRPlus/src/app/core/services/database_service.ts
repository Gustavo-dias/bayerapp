import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { FirebaseDatabase } from '@angular/fire';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  DATABASE: FirebaseDatabase = this.db.database;

  constructor(private db: AngularFireDatabase) { }

  connectDatabaseByCountry(): FirebaseDatabase {

    this.DATABASE = this.db.database.app.database(`https://rrplus-1044b.firebaseio.com`);

    return this.DATABASE;
  }

  getDatabaseRef(): FirebaseDatabase {
    return this.DATABASE;
  }

}