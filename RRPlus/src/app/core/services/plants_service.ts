import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MILHO, SOJA } from '../data/recomendacoes_inceticidas';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { DatabaseService } from './database_service';
import { FirebaseDatabase } from '@angular/fire';


@Injectable({
  providedIn: 'root'
})
export class PlantsService {

  databaseRef: FirebaseDatabase;

  constructor(
    private storage: AngularFireStorage,
    private db: AngularFireDatabase,
    private databaseService: DatabaseService) {
    this.databaseRef = this.databaseService.getDatabaseRef();
  }


  getAll(): Observable<any[]> {
    return this.db
      .list(this.databaseRef.ref("/plants"))
      .valueChanges();
  }

}
