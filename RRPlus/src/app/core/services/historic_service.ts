import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MILHO, SOJA } from '../data/recomendacoes_inceticidas';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { DatabaseService } from './database_service';
import { FirebaseDatabase } from '@angular/fire';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';


@Injectable({
    providedIn: 'root'
})
export class HistoricService {

    databaseRef: FirebaseDatabase;
    private PATH = 'historic/';
    private MONTHNAMES = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
    private WEEKNAMES = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"];

    constructor(
        private storage: AngularFireStorage,
        private db: AngularFireDatabase,
        private databaseService: DatabaseService,
        private uniqueDeviceID: UniqueDeviceID) {
        this.databaseRef = this.databaseService.getDatabaseRef();
    }


    newHistoric(title: any, text: any) {

        return new Promise((resolve, reject) => {

            this.uniqueDeviceID.get()
                .then((uuid: any) => {

                    var currentDate = new Date();

                    var data = {
                        uuid: uuid,
                        date: currentDate.getTime(),
                        title: title,
                        text: text,
                        day: currentDate.getDate(),
                        year: currentDate.getFullYear(),
                        monthName: this.MONTHNAMES[currentDate.getMonth()],
                        monthNameShort: (this.MONTHNAMES[currentDate.getMonth()]).substring(0, 3).toUpperCase(),
                        weekName: (this.WEEKNAMES[currentDate.getDay()]).substring(0, 3).toUpperCase(),
                        group: this.MONTHNAMES[currentDate.getMonth()] + ' ' + currentDate.getFullYear()
                    };

                    this.db.list(this.PATH)
                        .push(data)
                        .then(() => resolve())
                        .catch((e) => reject(e));


                })
                .catch((error: any) => console.log(error));

        })

    }



    getHistorics() {

        return new Promise((resolve, reject) => {

            this.uniqueDeviceID.get()
                .then((uuid: any) => {

                    return this.db
                        .list(this.databaseRef.ref("/historic"), ref => ref.orderByChild('uuid').equalTo(uuid))
                        .valueChanges().subscribe(data => {

                            var items: any = data;

                           var sortItems =  items.sort(function(a, b){
                                var keyA = new Date(a.date),
                                    keyB = new Date(b.date);

                                if(keyA > keyB) return -1;
                                if(keyA < keyB) return 1;
                                return 0;
                            });

                            resolve(sortItems);

                        });;

                });

        });
    }

    getHistoricsTest() {

        return new Promise((resolve, reject) => {

            var uuid = "e513441c-6ffd-f6bd-3582-400511111108";

            this.db
                .list(this.databaseRef.ref(`/historic/`)).valueChanges().subscribe(data => {

                    resolve(data);
                });;
        });
    }



}

