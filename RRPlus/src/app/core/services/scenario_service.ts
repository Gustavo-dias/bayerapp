import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MILHO, SOJA } from '../data/recomendacoes_inceticidas';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { DatabaseService } from './database_service';
import { FirebaseDatabase } from '@angular/fire';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';


@Injectable({
    providedIn: 'root'
})
export class ScenarioService {

    databaseRef: FirebaseDatabase;
    private PATH = 'scenario/';

    constructor(
        private storage: AngularFireStorage,
        private db: AngularFireDatabase,
        private databaseService: DatabaseService,
        private uniqueDeviceID: UniqueDeviceID) {
        this.databaseRef = this.databaseService.getDatabaseRef();
    }

    guid = {
        createGuid: function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        },
    }

    random(): number {
        let rand = Math.floor(Math.random() * 20000000) + 17;
        return rand;
    }

    dataAtualFormatada() {
        var data = new Date(),
            dia = data.getDate().toString(),
            diaF = (dia.length == 1) ? '0' + dia : dia,
            mes = (data.getMonth() + 1).toString(),
            mesF = (mes.length == 1) ? '0' + mes : mes,
            anoF = data.getFullYear();
        return diaF + "/" + mesF + "/" + anoF;
    }

    newScenario(recomendacaoData: any, type: any, text: any, cultura: any) {

        return new Promise((resolve, reject) => {

            this.uniqueDeviceID.get()
                .then((uuid: any) => {

                    var currentDate = new Date();

                    var data = {
                        id: this.guid.createGuid(),
                        uuid: uuid,
                        date: currentDate.getTime(),
                        title: 'Cenário ' + this.random(),
                        dateFormat: this.dataAtualFormatada(),
                        data: recomendacaoData,
                        type: type,
                        cultura: cultura,
                        favorite: false,
                        status: true,
                        textRecomendacao: text
                    };

                    this.db.list(this.PATH)
                        .push(data)
                        .then(() => resolve(data))
                        .catch((e) => reject(e));

                })
                .catch((error: any) => console.log(error));

        })

    }

    favoriteScenario(id: any) {

        return new Promise<any>((resolve, reject) => {

            var ref = this.databaseRef.ref("/scenario").orderByChild("id").equalTo(id);
            ref.once("value").then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {

                    childSnapshot.ref.update({
                        "favorite": true
                    }).then(function () {
                        resolve("success")
                    });
                });
            })

        });

    }

    updateNameScenario(id: any, title: any) {

        return new Promise<any>((resolve, reject) => {

            var ref = this.databaseRef.ref("/scenario").orderByChild("id").equalTo(id);
            ref.once("value").then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {

                    childSnapshot.ref.update({
                        "title": title
                    }).then(function () {
                        resolve("success")
                    });

                });
            })

        });

    }



    removeScenario(id: any) {

        return new Promise<any>((resolve, reject) => {

            var ref = this.databaseRef.ref("/scenario").orderByChild("id").equalTo(id);
            ref.once("value").then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {

                    childSnapshot.ref.update({
                        "status": false
                    }).then(function () {
                        resolve("success")
                    });

                });
            })

        });

    }

  

    getScenarios() {

        return new Promise((resolve, reject) => {

            this.uniqueDeviceID.get()
                .then((uuid: any) => {

                    return this.db
                        .list(this.databaseRef.ref("/scenario"), ref => ref
                            .orderByChild('uuid').equalTo(uuid))
                            .valueChanges().subscribe(data => {

                            var items: any = data;

                            items = items.filter(function (el) {
                                return (el.status === true);
                            });  

                            var sortItems = items.sort(function (a, b) {
                                var keyA = new Date(a.date),
                                    keyB = new Date(b.date);

                                if (keyA > keyB) return -1;
                                if (keyA < keyB) return 1;
                                return 0;
                            });

                            resolve(sortItems);

                        });

                });

        });
    }




}

