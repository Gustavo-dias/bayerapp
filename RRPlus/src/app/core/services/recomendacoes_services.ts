import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MILHO, SOJA, REFUGIO } from '../data/recomendacoes_inceticidas';


@Injectable({
  providedIn: 'root'
})
export class RecomendacoesService {


  constructor() {

  }


  getUnique(arr, comp) {

    const unique = arr
      .map(e => e[comp])
      .map((e, i, final) => final.indexOf(e) === i && i)
      .filter(e => arr[e]).map(e => arr[e]);

    return unique;
  }


  getMilhoById(data: any[], davis: any) {


    console.log('davis:'+davis);

    var milhos = MILHO.filter(function (milho) {
      return data.filter(function (datapass) {
        return milho.id == datapass;
      }).length > 0 || milho.escala_davis.indexOf(davis) > -1
    });




    return this.getUnique(milhos, 'praga');

  }

  getSojaById(data: any[]) {

    var sojas = SOJA.filter(function (soja) {
      return data.filter(function (datapass) {
        return soja.id == datapass;
      }).length > 0
    });

    return this.getUnique(sojas, 'praga');

  }

  pragasMilho(tratamento_semente: any, estadio_cultivo: any) {

    var itens = MILHO;

    for (let index = 0; index < itens.length; index++) {
      itens[index].status = false;
    }

    return this.getUnique(itens, 'praga');
  }

  pragasSoja(tratamento_semente: any, estadio_cultivo: any) {

    if (estadio_cultivo) {

      var itens = SOJA.filter(function (element) {
        return (element.estadio_cultivo.indexOf(estadio_cultivo) > -1);
      });

    }

    for (let i = 0; i < itens.length; i++) {

      itens[i].status = false;

    }

    return this.getUnique(itens, 'praga');

  }

  alertRefugio(crop: any) {

    var itens = REFUGIO.filter(function (element) {
      return (element.crop.indexOf(crop) > -1);
    });


    return itens;
  }

  alertRefugioPraga(crop: any, praga: any, remedio: any) {

    let filtered = remedio.map(a => a.name);
    filtered = filtered.map(v => v.toLowerCase().trim());

    var itens = REFUGIO.filter(function (element) {
      return (element.crop.indexOf(crop) > -1 && element.insetos.indexOf(praga) > -1);
    });


    console.log(JSON.stringify(filtered));

    if (itens.length > 0) {

      var message = itens[0].informacoes_adicionais;

      if (filtered.includes("belt")) {
        message += itens[0].belt;
      }
      if (filtered.includes("certero")) {
        message += itens[0].certero;
      }
      if (filtered.includes("larvin")) {
        message += itens[0].larvin;
      }
      if (filtered.includes("connect")) {
        message += itens[0].connect;
      }
      if (filtered.includes("bulldock")) {
        message += itens[0].bulldock;
      }
      if (filtered.includes("oberon")) {
        message += itens[0].oberon;
      }

      return message;

    }

    return "";

  }

  escalaMilho(tratamento_semente: any, estadio_cultivo: any, praga: any, type_escala: any) {

    var itens = MILHO.filter(function (element) {
      return (element.praga == praga);
    });

    return this.getUnique(itens, type_escala);

  }


  getMilhos(davis: any, injuria: any, praga: any) {

    if (davis && injuria) {

      return MILHO.filter(function (element) {
        return (element.escala_davis.indexOf(davis) > -1 && element.escala_injuria.indexOf(injuria) > -1 && element.praga == praga);
      });

    }
    else if (davis) {

      return MILHO.filter(function (element) {
        return (element.escala_davis.indexOf(davis) > -1 && element.praga == praga);
      });

    }
    else if (injuria) {

      return MILHO.filter(function (element) {
        return (element.escala_injuria.indexOf(injuria) > -1 && element.praga == praga);
      });
    }

  }



}
